﻿using System.Collections.Concurrent;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum PlanetType
    {
        Terrestrial,
        GiantGas,
        GiantIce,
        Count
    }

    class Planet
    {
        public PlanetType Type { get; set; }

        public bool IsHabitable { get; set; }

        public double Diameter { get; set; }

        public double Mass { get; set; }

        public double SemiMajorAxis { get; set; }

        public double OrbitalPeriod { get; set; }

        public double OrbitalEccentricity { get; set; }

        public double RotationPeriod { get; set; }
        
        public ConcurrentBag<Moon> Moons { get; private set; }
       
        public ConcurrentBag<Satellite> Satellites { get; private set; }

        public Atmosphere Atmosphere { get; set; }

        public PlanetaryRing Ring { get; set; }

        public Terrain Terrain { get; set; }

        public Planet()
        {
            Moons = new ConcurrentBag<Moon>();
            Satellites = new ConcurrentBag<Satellite>();
        }
    }
}
