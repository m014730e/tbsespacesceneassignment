﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    class AsteroidBelt
    {
        public List<Asteroid> Asteroids;

        public AsteroidBelt()
        {
            Asteroids = new List<Asteroid>();
        }

        public void AddAsteroid(Asteroid asteroid)
        {
            Asteroids.Add(asteroid);
        }

        public void AddAsteroids(List<Asteroid> asteroids)
        {
            Asteroids.AddRange(asteroids);
        }
    }
}
