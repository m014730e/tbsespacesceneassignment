﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class UniverseGenerator
    {
        public ConcurrentQueue<SolarSystem> Generate(int seed, int solarSystemCount)
        {
            ConcurrentQueue<SolarSystem> solarSystems = new ConcurrentQueue<SolarSystem>();

            RandomSingleton.Instance().SetSeed(seed);

            SolarSystemGenerator solarSystemGenerator = new SolarSystemGenerator();
            BlockingCollection<SolarSystem> blockingCollection = new BlockingCollection<SolarSystem>();
            Task producer = Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < solarSystemCount; i++)
                {
                   blockingCollection.Add(solarSystemGenerator.Generate(i));
                }
            }).ContinueWith((task)=>
            {
                //signal the production has ended
                blockingCollection.CompleteAdding();
            });

            Task consumer = Task.Factory.StartNew(() =>
            {
                while (!blockingCollection.IsCompleted)
                {
                    SolarSystem s;
                    if (blockingCollection.TryTake(out s))
                    {
                        solarSystems.Enqueue(s);
                    }
                }
            });

            consumer.Wait();
            return solarSystems;
        }
    }
}
