﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class SolarSystemGenerator
    {
        public SolarSystemGenerator() { }

        public SolarSystem Generate(int id)
        {
            RandomSingleton random = RandomSingleton.Instance();
            SolarSystem solarSystem = new SolarSystem(id);
            List<Task> tasks = new List<Task>();

            Task starTask = Task.Factory.StartNew(() =>
            {
                solarSystem.Star = new StarGenerator().Generate();
            });
            
            //need to wait for star generation
            Task planetTask = starTask.ContinueWith((prevTask) =>
            {
                PlanetGenerator planetGenerator = new PlanetGenerator();
                int planetCount = random.Next((int)Math.Sqrt((double)solarSystem.Star.Class), (int)Math.Sqrt(Math.Pow((double)solarSystem.Star.Class, 3.5)));
                for(int i = 0; i < planetCount; i++)
                {
                    solarSystem.Planets.Add(planetGenerator.Generate());                    
                }
            });

            Task cometTask = Task.Factory.StartNew(() =>
            {
                CometGenerator cometGenerator = new CometGenerator();
                int cometCount = random.Next(250);
                for (int i = 0; i < cometCount; i++)
                {
                    solarSystem.Comets.Add(cometGenerator.Generate());                  
                }
            });

            Task asteroidTask = Task.Factory.StartNew(() =>
            {
                double asteroidBeltChance = random.NextDouble();
                if (asteroidBeltChance < 0.2)
                    solarSystem.AsteroidBelt = new AsteroidBeltGenerator().Generate();
            });
            
            //no need to wait for star task
            Task.WaitAll(planetTask, cometTask, asteroidTask);

            return solarSystem;
        }
    }
}
