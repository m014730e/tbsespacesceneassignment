﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using TBSESpaceSceneGenerator.Generators;
using TBSESpaceSceneGenerator.Structures;
using TBSESpaceSceneGenerator.XML;

namespace TBSESpaceSceneGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title += " v" + App.Version;
        }

        private void btnGenerateSpaceScene_Click(object sender, RoutedEventArgs e)
        {
            ConcurrentQueue<SolarSystem> solarSystems = new ConcurrentQueue<SolarSystem>();
            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            bool isChecked = false;
            progressBar.Visibility = Visibility.Visible;
            progressBar.IsBusy = true;
            Stopwatch sw = new Stopwatch();
            int seed, solarSystemCount;
            try
            {
                seed = Convert.ToInt32(txtSeed.Text);
                solarSystemCount = Convert.ToInt32(txtSolarSystemCount.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error parsing textbox values.\n" + ex.Message, "Parsing Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            sw.Start();
            txtOutput.Text = "Generating Universe..." + Environment.NewLine;            
            Task.Factory.StartNew(() =>
            {
                Task producer = Task.Factory.StartNew(() =>
                {
                    solarSystems = new UniverseGenerator().Generate(seed, solarSystemCount);
                }).ContinueWith((task) =>
                {
                    txtOutput.Text += "Generated " + solarSystems.Count + " Solar Systems" + Environment.NewLine;

                    if (chkOutputXML.IsChecked == true)
                    {
                        txtOutput.Text += "Generating XML..." + Environment.NewLine;
                        isChecked = true;
                    }
                }, scheduler).ContinueWith((taskNew) =>
                {
                    if (isChecked == true)
                    {
                        SceneWriter.WriteUniverseFile(solarSystems);
                        SceneWriter.WriteSolarSystemFiles(solarSystems);
                    }
                }).ContinueWith((anotherTask)=>
                {
                    progressBar.IsBusy = false;
                    progressBar.Visibility = Visibility.Hidden;
                    txtOutput.Text += "Generated XML";
                    sw.Stop();
                    Console.WriteLine(sw.Elapsed);
                },scheduler);
            });            
        }
     }
}
